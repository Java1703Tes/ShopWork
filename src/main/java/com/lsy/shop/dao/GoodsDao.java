package com.lsy.shop.dao;

import cn.itcast.jdbc.TxQueryRunner;
import com.lsy.shop.domain.Goods;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import java.sql.SQLException;
import java.util.List;

public class GoodsDao {
    QueryRunner  queryRunner = new TxQueryRunner();

    public void add(Goods goods) {
        String sql = "insert into goods values(?,?,?,?,?,?,?,?)";
        Object[] parms  = {goods.getGid(),goods.getGname(),goods.getGprice(),goods.getGoffset(),goods.getGdescription(),goods.getUrl(),goods.getGnumber(),goods.getGcategory()};
        System.out.println(parms.toString());
        try {
            queryRunner.update(sql,parms);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Goods> findAll() {
        String sql = "select * from goods";
        try {
            List<Goods> goodsList = queryRunner.query(sql, new BeanListHandler<Goods>(Goods.class));
            return goodsList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void delete(String gid) {
        String sql = "delete from goods where gid = ?";
        Object[] parms = {gid};
        try {
            queryRunner.update(sql,parms);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
