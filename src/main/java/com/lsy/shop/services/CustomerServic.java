package com.lsy.shop.services;

import com.lsy.shop.dao.CustomerDao;
import com.lsy.shop.domain.Customer;
import com.lsy.shop.domain.PageBean;

/**
 * @author lenovo
 */
public class CustomerServic {
    private CustomerDao customerDao = new CustomerDao();

    /**
     * 添加客户
     */
    public void add(Customer customer) {
        customerDao.add(customer);
    }

    /**
     * 查找所有的客户
     */
    public PageBean<Customer> findAll(int pc, int ps)
    {
       return customerDao.findAll(pc,ps);
    }

    /**
     * 根据cid返回客户的信息
     * @param cid
     * @return
     */
    public Customer load(String cid) {
        return customerDao.findByid(cid);
    }

    /**
     * 对客户信息进行编辑
     * @param customer
     */
    public void edit(Customer customer) {
        customerDao.edit(customer);
    }


    /**
     * 根据cid来删除客户信息
     * @param cid
     */
    public void delete(String cid) {
        customerDao.delete(cid);
    }

    /**
     * 高级搜索返回的数据
     * @param customer
     * @param pc
     *@param ps @return
     */
    public PageBean<Customer> query(Customer customer, int pc, int ps) {
        return customerDao.query(customer,pc,ps);
    }
}
