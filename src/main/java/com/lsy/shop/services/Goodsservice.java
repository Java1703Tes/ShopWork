package com.lsy.shop.services;

import com.lsy.shop.dao.GoodsDao;
import com.lsy.shop.domain.Goods;
import com.lsy.shop.utils.ShopUtils;

import java.util.List;

public class Goodsservice {
    private GoodsDao goodsDao = new GoodsDao();

    public void add(Goods goods) {
        goodsDao.add(goods);
    }

    public List<Goods> findAll() {
        return goodsDao.findAll();
    }

    public void delete(String gid) {
        ShopUtils.delete(gid);
        goodsDao.delete(gid);
    }
}
