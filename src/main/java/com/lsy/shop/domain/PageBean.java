package com.lsy.shop.domain;

import java.util.List;

/**
 * @author lenovo
 */
public class PageBean<T>{
    private int pc;//当前页面的页码
    private int tr;//总页码数
    private int ps;//每页的记录数
    private List<T> beanList;//当前页的记录
    private String url;



    public String getUrl()
    {
        return url;
    }

    public int getTp()
    {
        int tp =  tr/ps;
        return tr % ps == 0 ? tp : tp+1;
    }

    public int getPc() {
        return pc;
    }

    public void setPc(int pc) {
        this.pc = pc;
    }

    public int getTr() {
        return tr;
    }

    public void setTr(int tr) {
        this.tr = tr;
    }

    public int getPs() {
        return ps;
    }

    public void setPs(int ps) {
        this.ps = ps;
    }

    public List<T> getBeanList() {
        return beanList;
    }

    public void setBeanList(List<T> beanList) {
        this.beanList = beanList;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "PageBean{" +
                "pc=" + pc +
                ", tr=" + tr +
                ", ps=" + ps +
                ", beanList=" + beanList +
                ", url='" + url + '\'' +
                '}';
    }
}
