package com.lsy.shop.domain;

public class Goods {
    private String gid;
    private String gname;
    private double gprice;
    private double goffset;
    private String gdescription;
    private String url;
    private String gnumber;
    private String gcategory;


    public String getGid() {
        return gid;
    }

    public void setGid(String gid) {
        this.gid = gid;
    }

    public String getGname() {
        return gname;
    }

    public void setGname(String gname) {
        this.gname = gname;
    }

    public double getGprice() {
        return gprice;
    }

    public void setGprice(double gprice) {
        this.gprice = gprice;
    }

    public double getGoffset() {
        return goffset;
    }

    public void setGoffset(double goffset) {
        this.goffset = goffset;
    }

    public String getGdescription() {
        return gdescription;
    }

    public void setGdescription(String gdescription) {
        this.gdescription = gdescription;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getGnumber() {
        return gnumber;
    }

    public void setGnumber(String gnumber) {
        this.gnumber = gnumber;
    }

    public String getGcategory() {
        return gcategory;
    }

    public void setGcategory(String gcategory) {
        this.gcategory = gcategory;
    }

    @Override
    public String toString() {
        return "Goods{" +
                "gid='" + gid + '\'' +
                ", gname='" + gname + '\'' +
                ", gprice=" + gprice +
                ", goffset=" + goffset +
                ", gdescription='" + gdescription + '\'' +
                ", url='" + url + '\'' +
                ", gnumber='" + gnumber + '\'' +
                ", gcategory='" + gcategory + '\'' +
                '}';
    }
}
