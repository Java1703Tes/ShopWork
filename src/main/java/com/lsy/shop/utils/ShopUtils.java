package com.lsy.shop.utils;

import cn.itcast.commons.CommonUtils;
import com.qiniu.common.QiniuException;
import com.qiniu.http.Response;
import com.qiniu.storage.BucketManager;
import com.qiniu.storage.UploadManager;
import com.qiniu.util.Auth;


public class ShopUtils {
    /**
     * 密钥
     */
    private static String AK = "o2eBItwCbaw4fE-MdachK2zZH3N-VcMEX3s7CDlb";
    private static String SK = "3ECTg_lTd-W3o1vVAMmTXLnypW-gqNAF8nZ-JB3b";
    /**
     * 上传文件的路径
     */
    private String Filepath = "D:\\8.png";
    /**
     * 上传的空间的名字
     */
    private static String bucketname = "mytest";
    /**
     * 上传文件后的名字
     */
    private String key = "hello.png";
    /**
     * 密钥设置
     */
    private static Auth auth = Auth.create(AK, SK);

    /**
     * 创建上传对象
     */
    private static UploadManager uploadManager = new UploadManager();

    /**
     * 简单的上传,只需要设置空间名即可(也可以带上key)
     *
     * @return
     */
    private static String getUpToken() {
        return auth.uploadToken(bucketname);
    }

    /**
     * 采用put的复杂上传
     */
    public static String upload(byte[] bytes) {
        try {
            String uuid = CommonUtils.uuid();
            Response response = uploadManager.put(bytes, uuid, getUpToken());
            return uuid;
        } catch (QiniuException e) {
            Response fail = e.response;
        }
        return null;
    }

    public static void delete(String gid) {
        BucketManager bucketMgr = new BucketManager(auth);
        try {
            bucketMgr.delete(bucketname, gid);
        } catch (QiniuException e) {
            e.printStackTrace();
        }
    }
}
