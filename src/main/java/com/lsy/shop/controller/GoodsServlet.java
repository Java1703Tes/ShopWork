package com.lsy.shop.controller;

import cn.itcast.servlet.BaseServlet;
import com.lsy.shop.domain.Goods;
import com.lsy.shop.services.Goodsservice;
import com.lsy.shop.utils.ShopUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class GoodsServlet extends BaseServlet{
    private Goodsservice goodsservice = new Goodsservice();

    public String findAll(HttpServletRequest request, HttpServletResponse response)
    {
        List<Goods> goodsList = goodsservice.findAll();
        Object[] array = goodsList.toArray();
        System.out.println(array.toString());
        request.setAttribute("gs",goodsList);
        return "f:/listgood.jsp";
    }

    public String delete(HttpServletRequest request,HttpServletResponse response)
    {
        String gid = request.getParameter("gid");
        goodsservice.delete(gid);
        request.setAttribute("msg","成功删除");
        return "f:/msg.jsp";
    }

}
