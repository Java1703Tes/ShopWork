package com.lsy.shop.controller;

import cn.itcast.commons.CommonUtils;
import cn.itcast.servlet.BaseServlet;
import com.lsy.shop.domain.Customer;
import com.lsy.shop.domain.PageBean;
import com.lsy.shop.services.CustomerServic;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;

/**
 * @author lenovo
 */
public class Customerservlet extends BaseServlet {
    private CustomerServic customerServic = new CustomerServic();

    /**
     * 添加客户,由于继承与BaseServlet,
     * 所以在客户访问servlet时,
     * 会自动的调用BaseServlet的Service方法
     * @param request
     * @param response
     * @return
     */
    public String add(HttpServletRequest request, HttpServletResponse response)
    {
        Customer customer = CommonUtils.toBean(request.getParameterMap(),Customer.class);
        customer.setCid(CommonUtils.uuid());
        System.out.println(customer.toString());
        customerServic.add(customer);
        request.setAttribute("msg","恭喜,添加客户成功!");
        return "f:/msg.jsp";
    }

    /**
     * 查看所有的客户
     * @param request
     * @param response
     * @return
     */
//    public String findAll(HttpServletRequest request,HttpServletResponse response)
//    {
//        request.setAttribute("cstmlist",customerServic.findAll());
//        return "f:/list.jsp";
//    }


    public String findAll(HttpServletRequest request,HttpServletResponse response)
    {
        int pc = getPc(request);
        int ps = 10;
        PageBean<Customer> pd = customerServic.findAll(pc,ps);
        pd.setUrl(getUrl(request));
        request.setAttribute("pd",pd);
        return "f:/list.jsp";
    }

    private int getPc(HttpServletRequest request) {
        String value = request.getParameter("pc");
        if(value == null || value.trim().isEmpty())
        {
            return 1;
        }
        return Integer.parseInt(value);
    }


    /**
     * 编辑之前的数据查找
     * @param request
     * @param response
     *
     * @return
     */
    public String preEdit(HttpServletRequest request,HttpServletResponse response)
    {
        String cid = request.getParameter("cid");
        Customer customer = customerServic.load(cid);
        request.setAttribute("cstm",customer);
        return "f:/edit.jsp";
    }


    /**
     * 对客户信息进行编辑(其实就是变相的保存)
     * @param request
     * @param response
     * @return
     */
    public String edit(HttpServletRequest request,HttpServletResponse response)
    {
        Customer customer = CommonUtils.toBean(request.getParameterMap(), Customer.class);
        customerServic.edit(customer);
        request.setAttribute("msg","恭喜,编辑客户成功!");
        return "f:/msg.jsp";
    }

    public String delete(HttpServletRequest request,HttpServletResponse response)
    {
        String cid = request.getParameter("cid");
        customerServic.delete(cid);
        request.setAttribute("msg","成功删除");
        return "f:/msg.jsp";
    }


    /**
     * 多条件的查找
     * @param request
     * @param response
     * @return
     */
//    public String query (HttpServletRequest request,HttpServletResponse response)
//    {
//        Customer customer = CommonUtils.toBean(request.getParameterMap(), Customer.class);
//        List<Customer> cstm = customerServic.query(customer);
//        request.setAttribute("cstmlist",cstm);
//        return "f:/list.jsp";
//    }

    /**
     * 加入url后的更改,防止在搜索过程中的url丢失
     * @param request
     * @param response
     * @return
     */
    public String query (HttpServletRequest request,HttpServletResponse response) throws UnsupportedEncodingException {
        String url = getUrl(request);
        int pc = getPc(request);
        int ps = 10;
        Customer customer = CommonUtils.toBean(request.getParameterMap(), Customer.class);
        customer = ecoding(customer);
        PageBean<Customer> pb = customerServic.query(customer,pc,ps);
        pb.setUrl(url);
        request.setAttribute("pd",pb);
        System.out.println();
        return "f:/list.jsp";
    }

    private Customer ecoding(Customer customer) throws UnsupportedEncodingException {
        customer.setCname(new String(customer.getCname().getBytes("ISO-8859-1"),"utf-8"));
        customer.setGender(new String(customer.getGender().getBytes("ISO-8859-1"),"utf-8"));
        customer.setEmail(new String(customer.getEmail().getBytes("ISO-8859-1"),"utf-8"));
        customer.setCellphone(new String(customer.getCellphone().getBytes("ISO-8859-1"),"utf-8"));
        return customer;
    }


    /**
     * 截取url
     * @param request
     * @return
     */
    public String getUrl(HttpServletRequest request)
    {
        String contextPath = request.getContextPath();
        String servletPath = request.getServletPath();
        String queryString = request.getQueryString();
        if(queryString.contains("&pc="))
        {
            int index = queryString.lastIndexOf("&pc=");
            queryString = queryString.substring(0,index);
        }
        System.out.println(contextPath + servletPath + "?" + queryString);
        return contextPath + servletPath + "?" + queryString;
    }
}
