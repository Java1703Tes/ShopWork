package com.lsy.shop.controller;

import cn.itcast.servlet.BaseServlet;
import com.lsy.shop.domain.Goods;
import com.lsy.shop.services.Goodsservice;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class ShopServlet extends BaseServlet{
    private Goodsservice goodsservice = new Goodsservice();

    public String findAll(HttpServletRequest request, HttpServletResponse response)
    {
        List<Goods> goodsList = goodsservice.findAll();
        request.setAttribute("gs",goodsList);
        return "f:/shop.jsp";
    }
}
