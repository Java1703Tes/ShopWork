package com.lsy.shop.controller;

import cn.itcast.commons.CommonUtils;
import cn.itcast.servlet.BaseServlet;
import com.lsy.shop.domain.Goods;
import com.lsy.shop.services.Goodsservice;
import com.lsy.shop.utils.ShopUtils;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GoodsAddservlet extends HttpServlet {
    private Goodsservice goodsservice = new Goodsservice();


    /**
     * 解析请求头并且将文件上传
     *
     * @param request
     * @return
     */
    private Map<Object, Object> getParms(HttpServletRequest request) {
        Map parms = new HashMap<Object, Object>();
        /**
         * 创建工厂解析器
         * 获得ServletFIleupload对象
         * 遍历fileitems判断哪个是文件信息,并将文件信息上传到七牛云
         */
        DiskFileItemFactory factory = new DiskFileItemFactory();
        ServletFileUpload upload = new ServletFileUpload(factory);
        upload.setHeaderEncoding("UTF-8");
        try {
            List<FileItem> fileItems = upload.parseRequest(request);
            for (FileItem fileItem : fileItems) {
                if (fileItem.isFormField()) {
                    String name = fileItem.getFieldName();
                    System.out.println(name);
                    String value = fileItem.getString("utf-8");
                    System.out.println(value);
                    parms.put(name, value);
                } else {
                    System.out.println(fileItem.getName());
                    byte[] bytes = new byte[100000];
                    InputStream inputStream = fileItem.getInputStream();
                    inputStream.read(bytes);
                    String gid = ShopUtils.upload(bytes);
                    String url = "http://p04oo1nz3.bkt.clouddn.com/" + gid;
                    System.out.println(url);
                    parms.put("url", url);
                    parms.put("gid",gid);
                }
            }

        } catch (FileUploadException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return parms;
    }


    /**
     * 添加商品
     *
     * @param request
     * @param response
     * @return
     */
    public void add(HttpServletRequest request, HttpServletResponse response) {
        /**
         * 1.解析请求的参数
         */
        try {
            Map<Object, Object> parms = getParms(request);
            System.out.println(parms.toString());
            Goods goods = CommonUtils.toBean(parms, Goods.class);
            System.out.println(goods.toString());
            goodsservice.add(goods);
            request.setAttribute("msg", "成功,添加商品");
            request.getRequestDispatcher("/msg.jsp").forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=UTF-8");
        req.setCharacterEncoding("UTF-8");
        add(req, resp);
    }
}
