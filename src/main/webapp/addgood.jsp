<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: lenovo
  Date: 2017/12/3
  Time: 13:58
  To change this template use File | Settings | File Templates.
--%>
<html>
<head>
    <title>Title</title>
</head>
<body>
    <h3 style="text-align: center">添加商品</h3>
<form action="<c:url value="/GoodsAddservlet"/>" method="post" enctype="multipart/form-data">
    <table border="0" align="center" width="40%" style="margin-left: 100px;">
        <tr>
            <td width="100px">商品名称</td>
            <td width="40%">
                <input type="text" name="gname"/>
            </td>
        </tr>
        <tr>
            <td>价钱</td>
            <td>
                <input type="text" name="gprice" />
            </td>
        </tr>
        <tr>
            <td>商品分类</td>
            <td>
                <input type="radio" name="gcategory" value="外衣"/>&nbsp;外衣
                <input type="radio" name="gcategory" value="鞋子"/>&nbsp;鞋子
                <input type="radio" name="gcategory" value="帽子"/>&nbsp;帽子
            </td>
        </tr>
        <tr>
            <td>商品数量</td>
            <td>
                <input type="text" name="gnumber"/>
            </td>
        </tr>
        <tr>
            <td>折扣</td>
            <td>
                <input type="text" name="goffset"/>
            </td>
        </tr>
        <tr>
            <td>商品图片</td>
            <td>
                <input type="file" name="picture">
            </td>
        </tr>
        <tr>
            <td>商品描述</td>
            <td>
                <textarea rows="5" cols="30" name="gdescription"></textarea>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <input type="submit" value="添加商品"/>
                <input type="reset" value="重置"/>
            </td>
            <td>&nbsp;</td>
        </tr>
    </table>
</form>
</body>
</html>
