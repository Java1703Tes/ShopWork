<%--
  Created by IntelliJ IDEA.
  User: lenovo
  Date: 2017/12/1
  Time: 20:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" href="style.css"/>
    <script src="jquery/jquery-1.5.1.js"></script>
</head>
<body>
<form action="">
    <div class="logintxt">
        <span>用户名:<input style="height: 20px;width: 220px" type="text" name="username"/></span>
        <span>密　码:<input style="height: 20px;width: 220px" type="password" name="password"/></span>
        <span><button style="height: 30px;width: 70px" type="submit">登录</button></span>
        <a href=""><span><button style="height: 30px;width: 70px">注册</button></span></a>
        <span class="close">X</span>

    </div>
</form>
<div class="block">
    <div class="title">
        <span><img src="./img/title.png" alt=""/></span>
        <span><img src="./img/shopingc.png" alt="" /></span>
        <span class="click"><img src="./img/login.png" alt=""/></span>
    </div>
    <div class="bannerblock">
        <img src="./img/banner.png" width="100%" height="300px" alt=""/>
    </div>
    <div class="choose">
        <span>FILTE BY:</span>
        <span><a href="<c:url value="/ShopServlet?method=findbyCategory&gcategory=裤子"/>">pants</a></span>
        <span><a href="<c:url value="/ShopServlet?method=findbyCategory&gcategory=鞋子"/>">shoes</a></span>
        <span><a href="<c:url value="/ShopServlet?method=findbyCategory&gcategory=帽子"/>">cap</a></span>
        <span><a href="<c:url value="/ShopServlet?method=findbyCategory&gcategory=衣服"/>">clothes</a></span>
    </div>
    <div class="showproduct">
        <div class="line">
            <c:forEach items="${requestScope.gs}" var="g">
                <div class="products">
                    <span><img src="${g.url}" alt=""/></span>
                    <span>${g.gname}</span>
                    <span>${g.gdescription}</span>
                    <span>$${g.gprice*g.goffset}</span>
                    <div class="new">
                        <c:if test="${g.goffset<1}">
                            <img src="img/flag.png" width="70px" height="27px" alt="">
                        </c:if>
                    </div>
                </div>
            </c:forEach>
        </div>
    </div>
    <div class="footer">
        <span><img src="./img/footer.png" alt=""/></span>
    </div>
</div>
<script>
    $(function () {
        $(".click").click(function () {
            $(".logintxt").css({
                "display": "block"
            })
        })
        $(".close").click(function () {
            $(".logintxt").css({
                "display": "none"
            })
        })
    })
</script>
</body>
</html>