<%@ page contentType="text/html;charset=UTF-8" language="java" import="java.lang.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>客户列表</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
<h3 align="center">客户列表</h3>
<table border="1" width="70%" align="center">
	<tr>
		<th>客户姓名</th>
		<th>账号</th>
		<th>密码</th>
		<th>性别</th>
		<th>生日</th>
		<th>手机</th>
		<th>邮箱</th>
		<th>描述</th>
		<th>操作</th>
	</tr>
	<c:forEach items="${pd.beanList}" var="cstm">
		<tr>
			<td>${cstm.cname}</td>
			<td>${cstm.username}</td>
			<td>${cstm.password}</td>
			<td>${cstm.gender}</td>
			<td>${cstm.birthday}</td>
			<td>${cstm.cellphone}</td>
			<td>${cstm.email}</td>
			<td>${cstm.description}</td>
			<td>
				<a href="<c:url value='/CustomerServlet?method=preEdit&cid=${cstm.cid}'/>">编辑</a>
				<a href="<c:url value='/CustomerServlet?method=delete&cid=${cstm.cid}'/>">删除</a>
			</td>
		</tr>
	</c:forEach>
</table>
 <p style="text-align: center">
	  第${pd.pc}页/共${pd.tp}页
	  <a href="${pd.url}&pc=1">首页</a>
	 <c:if test="${pd.pc>1}">
		 <a href="${pd.url }&pc=${pd.pc-1}">上一页</a>
	 </c:if>

	 <c:choose>
		 <c:when test="${pd.tp<10}">
			 <c:set value="1" var="begin"/>
			 <c:set var="end" value="${pd.tp}"/>
		 </c:when>
	 
	 	<c:otherwise>
		 	<c:set var="begin" value="${pd.pc-5}"/>
		 	<c:set var="end" value="${pd.pc+4}"/>
		 	<c:if test="${begin<1}">
				 <c:set value="1" var="begin"/>
                 <c:set value="10" var="end"/>
			 </c:if>
		 	<c:if test="${end>pd.tp}">
			     <c:set var="end" value="${pd.tp}"/>
                 <c:set var="begin" value="${pd.tp- 9}"/>
		 	</c:if>
	 	</c:otherwise>
	 </c:choose>
	 <c:forEach var="i" begin="${begin}" end="${end}">
		 <c:choose>
			 <c:when test="${i eq pd.pc}">
				 [${i}]
			 </c:when>
			 <c:otherwise>
				 <a href="${pd.url}&pc=${i}">[${i}]</a>
			 </c:otherwise>
		 </c:choose>
	 </c:forEach>

	 <c:if test="${pd.pc < pd.tp}">
		 <a href="${pd.url}&pc=${pd.pc+1}">下一页</a>
	 </c:if>
	  <a href="${pd.url}&pc=${pd.tp}">尾页</a>
 </p>
  </body>
</html>
