<%--
  Created by IntelliJ IDEA.
  User: lenovo
  Date: 2017/12/3
  Time: 16:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h3 align="center">商品列表</h3>
<table border="1" width="70%" align="center">
    <tr>
        <th>商品名称</th>
        <th>商品价钱</th>
        <th>商品图片</th>
        <th>商品数量</th>
        <th>商品分类</th>
        <th>商品折扣</th>
        <th>打折价钱</th>
        <th>描述描述</th>
        <th>操作</th>
    </tr>
    <c:forEach items="${requestScope.gs}" var="g">
        <tr>
            <td>${g.gname}</td>
            <td>${g.gprice}</td>
            <td><img src="${g.url}" width="100px" height="100px"></td>
            <td>${g.gnumber}</td>
            <td>${g.gcategory}</td>
            <td>${g.goffset}</td>
            <td>${g.gprice*g.goffset}</td>
            <td>${g.gdescription}</td>
            <td>
                <a href="<c:url value='/Goodsservlet?method=delete&gid=${g.gid}'/>">删除</a>
            </td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
